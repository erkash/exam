<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Comment
 *
 * @ORM\Table(name="comment")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CommentRepository")
 */
class Comment
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Restaurant", inversedBy="comments")
     */
    private $restaurant;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="comments")
     */
    private $user;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;

    /**
     * @var int
     *
     * @ORM\Column(name="quality_of_food", type="integer")
     */
    private $qualityOfFood;

    /**
     * @var int
     *
     * @ORM\Column(name="service_quality", type="integer")
     */
    private $serviceQuality;

    /**
     * @var int
     *
     * @ORM\Column(name="interior", type="integer")
     */
    private $interior;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="publish_date", type="datetime")
     */
    private $publishDate;
    
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set restaurant
     *
     * @param \AppBundle\Entity\Restaurant $restaurant
     *
     * @return Comment
     */
    public function setRestaurant(\AppBundle\Entity\Restaurant $restaurant = null)
    {
        $this->restaurant = $restaurant;

        return $this;
    }

    /**
     * Get restaurant
     *
     * @return \AppBundle\Entity\Restaurant
     */
    public function getRestaurant()
    {
        return $this->restaurant;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return Comment
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return Comment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set qualityOfFood
     *
     * @param integer $qualityOfFood
     *
     * @return Comment
     */
    public function setQualityOfFood($qualityOfFood)
    {
        $this->qualityOfFood = $qualityOfFood;

        return $this;
    }

    /**
     * Get qualityOfFood
     *
     * @return integer
     */
    public function getQualityOfFood()
    {
        return $this->qualityOfFood;
    }

    /**
     * Set serviceQuality
     *
     * @param integer $serviceQuality
     *
     * @return Comment
     */
    public function setServiceQuality($serviceQuality)
    {
        $this->serviceQuality = $serviceQuality;

        return $this;
    }

    /**
     * Get serviceQuality
     *
     * @return integer
     */
    public function getServiceQuality()
    {
        return $this->serviceQuality;
    }

    /**
     * Set interior
     *
     * @param integer $interior
     *
     * @return Comment
     */
    public function setInterior($interior)
    {
        $this->interior = $interior;

        return $this;
    }

    /**
     * Get interior
     *
     * @return integer
     */
    public function getInterior()
    {
        return $this->interior;
    }

    /**
     * Set publishDate
     *
     * @param \DateTime $publishDate
     *
     * @return Comment
     */
    public function setPublishDate($publishDate)
    {
        $this->publishDate = $publishDate;

        return $this;
    }

    /**
     * Get publishDate
     *
     * @return \DateTime
     */
    public function getPublishDate()
    {
        return $this->publishDate;
    }
}
