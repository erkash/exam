<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Comment;
use AppBundle\Entity\Image;
use AppBundle\Entity\Restaurant;
use AppBundle\Form\CommentType;
use AppBundle\Form\ImageType;
use AppBundle\Form\removeCommentType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;

class BasicController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction()
    {
        $restaurants = $this->getDoctrine()
            ->getRepository('AppBundle:Restaurant')
            ->findAll();

        return $this->render('AppBundle:Basic:index.html.twig', [
            'restaurants' => $restaurants
        ]);
    }

    /**
     * @Route("/restaurants/new")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function newRestaurantAction(Request $request)
    {
        /** @var Restaurant $restaurant */
        $restaurant = new Restaurant();

        $form_builder = $this->createFormBuilder($restaurant);

        $form_builder->add('name', TextType::class);
        $form_builder->add('category', EntityType::class,[
            'class' => 'AppBundle\Entity\Category']);
        $form_builder->add('description', TextareaType::class);
        $form_builder->add('imageFile', FileType::class, ['label' => 'Main photo']);
        $form_builder->add('save', SubmitType::class, ['label' => 'Submit new place']);

        $form = $form_builder->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $restaurant = $form->getData();
            $restaurant->setUser($this->getUser());

            $em = $this->getDoctrine()->getManager();
            $em->persist($restaurant);
            $em->flush();

            return $this->redirectToRoute("app_basic_detail", ['id' => $restaurant->getId()]);
        }
        return $this->render('AppBundle:Basic:new.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/restaurant/{id}", requirements={"id": "\d+"})
     * @param $id integer
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function detailAction(int $id, Request $request) {
        $restaurant = $this->getDoctrine()
            ->getRepository('AppBundle:Restaurant')
            ->find($id);

        $images = $restaurant->getImages();
        /** @var Comment[] $comments */
        $comments = $restaurant->getComments();

        $deleteComment = [];

        foreach ($comments as $comment) {
            $deleteComment[$comment->getId()] = $this->createForm(removeCommentType::class,null,[
                'method' => 'DELETE',
                'action' => $this->generateUrl('app_basic_delete', ['id'=> $comment->getId()])
            ])->createView();
        }

        $comment = new Comment();

        $comment_form = $this->createForm(CommentType::class, $comment);
        $comment_form->handleRequest($request);

        $comment_twig = $comment_form->createView();

        foreach ($comments as $comment) {
            if ($comment->getUser() == $this->getUser()) {
                $comment_twig = null;
            }
        }

        if ($comment_form->isSubmitted() && $comment_form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $comment->setUser($this->getUser());
            $comment->setRestaurant($restaurant);
            $comment->setPublishDate(new \DateTime());

            $em->persist($comment);
            $em->flush();
        }

        $image = new Image();
        $image_form = $this->createForm(ImageType::class, $image);
        $image_form->handleRequest($request);

        if ($image_form->isSubmitted() && $image_form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $image->setUser($this->getUser());
            $image->setRestaurant($restaurant);

            $em->persist($image);
            $em->flush();
        }

        return $this->render('AppBundle:Basic:detail.html.twig', [
            'restaurant' => $restaurant,
            'images' => $images,
            'comment_form' => $comment_twig,
            'image_form' => $image_form->createView(),
            'comments' => $comments,
            'deleteComment' => $deleteComment
        ]);
    }

    /**
     * @Route("/search")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Method({"POST"})
     */
    public function searchAction(Request $request) {
        $data = $request->request->get(trim('data'));

        $restaurants = $this->getDoctrine()->getRepository('AppBundle:Restaurant')->findRestaurant($data);

        return $this->render('AppBundle:Basic:search_results.html.twig', array(
            'restaurants' => $restaurants
        ));
    }

    /**
     * @Route("/comment/{id}", requirements={"id": "\d+"})
     * @Method({"DELETE"})
     * @param $id integer
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function deleteAction(int $id)
    {
        $comment = $this->getDoctrine()
            ->getRepository('AppBundle:Comment')
            ->find($id);

        $id = $comment->getRestaurant()->getId();
        $em = $this->getDoctrine()->getManager();
        $em->remove($comment);
        $em->flush();
        return $this->redirectToRoute("app_basic_detail", [
            'id' => $id]);
    }

}
