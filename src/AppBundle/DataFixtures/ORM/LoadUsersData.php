<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class LoadUsersData extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $users = [
            [
                'username' => 'user0',
                'email' => 'doctor0@ya.ru',
                'name' => 'John Doe'
            ],
            [
                'username' => 'user1',
                'email' => 'doctor1@ya.ru',
                'name' => 'Jane Nord'
            ],
            [
                'username' => 'user2',
                'email' => 'doctor2@ya.ru',
                'name' => 'Noisie Nimad'
            ],
            [
                'username' => 'user3',
                'email' => 'doctor3@ya.ru',
                'name' => 'Marta Ogirera'
            ],
            [
                'username' => 'user4',
                'email' => 'doctor4@ya.ru',
                'name' => 'Uriayh Faber'
            ],
            [
                'username' => 'user5',
                'email' => 'doctor5@ya.ru',
                'name' => 'Alan Smith'
            ],
            [
                'username' => 'user6',
                'email' => 'doctor6@ya.ru',
                'name' => 'James Gunn'
            ]
        ];

        foreach ($users as $value)
        {
            $user = new User();

            $user->setUsername($value['username']);
            $user->setEmail($value['email']);
            $user->setName($value['name']);
            $user->setRoles(['ROLE_USER']);
            $user->setEnabled(true);
            $encoder = $this->container->get('security.password_encoder');
            $password = $encoder->encodePassword($user, '123');
            $user->setPassword($password);

            $manager->persist($user);
        }

        $manager->flush();
    }
}